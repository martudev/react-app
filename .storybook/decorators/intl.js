import React from 'react';
import { IntlProvider } from 'react-intl';

import en from '../../src/i18n/en.json';
import es from '../../src/i18n/es.json';

const messages = {
  en,
  es,
};
const DEFAULT_LOCALE = 'en';

const locale =
  (navigator.languages && navigator.languages[0]) || navigator.language;

const localeWithoutRegion = locale.toLowerCase().split(/[_-]+/)[0];

const messagesLocale =
  messages[localeWithoutRegion] || messages[locale] || messages[DEFAULT_LOCALE];

export default (storyFn) => {
  return (
    <IntlProvider locale={locale} messages={messages[locale]}>
      {storyFn()}
    </IntlProvider>
  );
};
