import React from "react";
import { addDecorator } from "@storybook/react";
import { MemoryRouter } from "react-router";
import IntlDecorator from "./decorators/intl";
import withFormik from "storybook-formik";

addDecorator(withFormik);

addDecorator(IntlDecorator);

addDecorator((story) => (
  <MemoryRouter initialEntries={["/"]}>{story()}</MemoryRouter>
));

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};
