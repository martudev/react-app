import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider } from 'react-intl';

import './index.css';
import App from './App';
import en from './i18n/en';
import es from './i18n/es';

const messages = {
  en,
  es,
};
const DEFAULT_LOCALE = 'en';

const locale =
  (navigator.languages && navigator.languages[0]) || navigator.language;

const localeWithoutRegion = locale.toLowerCase().split(/[_-]+/)[0];

const messagesLocale =
  messages[localeWithoutRegion] || messages[locale] || messages[DEFAULT_LOCALE];

ReactDOM.render(
  <React.StrictMode>
    <IntlProvider locale={locale} messages={messagesLocale}>
      <App />
    </IntlProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
