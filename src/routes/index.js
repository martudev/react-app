import React from 'react';
import { Route } from 'react-router-dom';

import Home from 'pages/home';
import Example1 from 'pages/example1';
import { Example2 } from 'pages/example2';
import { Search } from 'pages/search';
import { Payment } from 'pages/payment';
import { Success } from 'pages/success';

export const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/example-1',
    component: Example1,
  },
  {
    path: '/example-2',
    component: Example2,
  },
  {
    path: '/search',
    component: Search,
  },
  {
    path: '/payment',
    component: Payment,
  },
  {
    path: '/success',
    component: Success,
  },
];

export function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={(props) => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

export default routes;
