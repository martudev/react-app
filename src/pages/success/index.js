import React, { useState, useEffect } from 'react';
import { useStripe, Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';

import { client, requestBuilder } from '../payment';

const stripePromise = loadStripe(
  'pk_test_51KIz8vFWsQK9wZRJ8MqV56j55Zi6nGooCp9QleNHmV6pRdiRBtb8Aqb0yMjO2gA75o8dutKzFYRAbZne1rEOaovx00CaM04l0j'
);

export function PaymentSuccess() {
  const stripe = useStripe();
  const [message, setMessage] = useState(null);

  const paymentSuccess = async (customerId) => {
    try {
      const response = await client.execute({
        uri: requestBuilder.payments.parse({}).build(),
        method: 'POST',
        body: {
          amountPlanned: {
            type: 'centPrecision',
            currencyCode: 'USD',
            centAmount: 1099,
            fractionDigits: 2,
          },
          custom: {
            type: {
              key: 'stripe-category1',
            },
            fields: {
              customerId: customerId,
            },
          },
        },
      });
      if (response.statusCode === 201) {
        const results = response.body.results;
        console.log(results);
        setMessage('Success! Your payment method has been saved.');
      }
    } catch (e) {
      setMessage('Some gone wrong');
    }
  };

  useEffect(() => {
    if (!stripe) {
      return;
    }

    // Retrieve the "setup_intent_client_secret" query parameter appended to
    // your return_url by Stripe.js
    const urlSearch = new URLSearchParams(window.location.search);
    const clientSecret = urlSearch.get('setup_intent_client_secret');
    const customerId = urlSearch.get('customerId');

    // Retrieve the SetupIntent
    stripe.retrieveSetupIntent(clientSecret).then(({ setupIntent }) => {
      // Inspect the SetupIntent `status` to indicate the status of the payment
      // to your customer.
      //
      // Some payment methods will [immediately succeed or fail][0] upon
      // confirmation, while others will first enter a `processing` state.
      //
      // [0]: https://stripe.com/docs/payments/payment-methods#payment-notification
      switch (setupIntent.status) {
        case 'succeeded':
          paymentSuccess(customerId);
          break;

        case 'processing':
          setMessage(
            "Processing payment details. We'll update you when processing is complete."
          );
          break;

        case 'requires_payment_method':
          // Redirect your user back to your payment page to attempt collecting
          // payment again
          setMessage(
            'Failed to process payment details. Please try another payment method.'
          );
          break;
      }
    });
  }, [stripe]);

  return message;
}

export function Success() {
  return (
    <Elements stripe={stripePromise}>
      <PaymentSuccess />
    </Elements>
  );
}
