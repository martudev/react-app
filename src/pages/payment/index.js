import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createClient } from '@commercetools/sdk-client';
import { createHttpMiddleware } from '@commercetools/sdk-middleware-http';
import { createAuthMiddlewareForClientCredentialsFlow } from '@commercetools/sdk-middleware-auth';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import CheckoutForm from './CheckoutForm';

import './index.scss';
import { useEffect, useRef, useState } from 'react';

const apiUrl = process.env.REACT_APP_CT_API_URL;
const authUrl = process.env.REACT_APP_CT_AUTH_URL;
const projectKey = process.env.REACT_APP_CT_PROJECT_KEY;
const clientId = process.env.REACT_APP_CT_CLIENT_ID;
const clientSecret = process.env.REACT_APP_CT_CLIENT_SECRET;

export const requestBuilder = createRequestBuilder({ projectKey });
export const client = createClient({
  middlewares: [
    createAuthMiddlewareForClientCredentialsFlow({
      host: authUrl,
      projectKey: projectKey,
      credentials: {
        clientId: clientId,
        clientSecret: clientSecret,
      },
    }),
    createHttpMiddleware({ host: apiUrl }),
  ],
});

const stripePromise = loadStripe(
  'pk_test_51KIz8vFWsQK9wZRJ8MqV56j55Zi6nGooCp9QleNHmV6pRdiRBtb8Aqb0yMjO2gA75o8dutKzFYRAbZne1rEOaovx00CaM04l0j'
);

export function Payment() {
  const [clientSecretStripe, setClientSecretStripe] = useState(null);
  const [customerId, setCustomerId] = useState('');

  useEffect(() => {
    (async () => {
      //https://extreme-battery-338715.uc.r.appspot.com
      const customerResponse = await fetch(
        'http://localhost:4444/stripe/createCustomer',
        {
          method: 'POST',
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        }
      );
      const customer = await customerResponse.json();
      const response = await fetch('http://localhost:4444/payment/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
          customerId: customer.id,
        }),
      });
      const { client_secret: clientSecret } = await response.json();
      // Render the Payment Element using the clientSecret
      setCustomerId(customer.id);
      setClientSecretStripe(clientSecret);
    })();
  }, []);

  const options = {
    // passing the client secret obtained in step 2
    clientSecret: clientSecretStripe,
    // Fully customizable with appearance API.
    appearance: {
      /*...*/
    },
  };

  return (
    <>
      {clientSecretStripe && (
        <Elements stripe={stripePromise} options={options}>
          <CheckoutForm customerId={customerId} />
        </Elements>
      )}
    </>
  );
}
