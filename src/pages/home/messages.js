import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'home.title',
    defaultMessage: 'Devgurus template',
  },
  linkOne: {
    id: 'home.linkOne',
    defaultMessage: 'Example 1',
  },
  linkTwo: {
    id: 'home.linkTwo',
    defaultMessage: 'Example 2',
  },
  linkThree: {
    id: 'home.linkThree',
    defaultMessage: 'Search',
  },
  linkFour: {
    id: 'home.linkFour',
    defaultMessage: 'Payment',
  },
});

export default messages;
