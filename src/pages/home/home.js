import React from 'react';
import { FormattedMessage } from 'react-intl';
import { CustomLink } from 'common/ui-kit';

import styles from './home.module.scss';
import messages from './messages';
import DefaultTemplate from '../../common/layouts/default';

export const Home = () => {
  return (
    <DefaultTemplate title={<FormattedMessage {...messages.title} />}>
      <ul className={styles['two-columns']}>
        <li>
          <CustomLink to="/example-1">
            <FormattedMessage {...messages.linkOne} />
          </CustomLink>
        </li>
        <li>
          <CustomLink to="/example-2">
            <FormattedMessage {...messages.linkTwo} />
          </CustomLink>
        </li>
        <li>
          <CustomLink to="/search">
            <FormattedMessage {...messages.linkThree} />
          </CustomLink>
        </li>
        <li>
          <CustomLink to="/payment">
            <FormattedMessage {...messages.linkFour} />
          </CustomLink>
        </li>
      </ul>
    </DefaultTemplate>
  );
};

Home.displayName = 'Home';
Home.propTypes = {};

export default Home;
