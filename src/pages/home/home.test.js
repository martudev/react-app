import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { render, screen } from '@testing-library/react';
import Home from './index.js';

test('renders learn react link', () => {
  render(
    <IntlProvider locale={'en'}>
      <Router>
        <Home />
      </Router>
    </IntlProvider>
  );
  const linkElement = screen.getByText(/Devgurus template/i);
  expect(linkElement).toBeInTheDocument();
});
