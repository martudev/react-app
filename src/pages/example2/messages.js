import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'example2.title',
    defaultMessage: 'Example 2 Page',
  },
  firstName: {
    id: 'example2.firstName',
    defaultMessage: 'First name',
  },
  lastName: {
    id: 'example2.lastName',
    defaultMessage: 'Last name',
  },
  submitButton: {
    id: 'example2.submitButton',
    defaultMessage: 'Save',
  },
});

export default messages;
