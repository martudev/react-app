import React from 'react';
import { Formik, Form } from 'formik';
import { useIntl, FormattedMessage } from 'react-intl';

import { TextInput } from 'common/ui-kit';

import styles from './example2.module.css';
import { validate } from './validation';
import messages from './messages';

export const Example2 = () => {
  const intl = useIntl();

  return (
    <div>
      <h1 className={styles.example}>
        <FormattedMessage {...messages.title} />
      </h1>
      <h2>This page has not a layout.</h2>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
        }}
        validate={validate}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(true);
          await new Promise((r) => setTimeout(r, 500));
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <div>
              <TextInput
                label={intl.formatMessage(messages.firstName)}
                name="firstName"
                type="text"
                placeholder="Jane"
              />
            </div>
            <div>
              <TextInput
                label={intl.formatMessage(messages.lastName)}
                name="lastName"
                type="text"
                placeholder="Doe"
              />
            </div>
            <button disabled={isSubmitting} type="submit">
              {intl.formatMessage(messages.submitButton)}
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Example2;
