import { createRequestBuilder } from '@commercetools/api-request-builder';
import { createClient } from '@commercetools/sdk-client';
import { createHttpMiddleware } from '@commercetools/sdk-middleware-http';
import { createAuthMiddlewareForClientCredentialsFlow } from '@commercetools/sdk-middleware-auth';

import './index.scss';
import { useEffect, useRef, useState } from 'react';

const apiUrl = process.env.REACT_APP_CT_API_URL;
const authUrl = process.env.REACT_APP_CT_AUTH_URL;
const projectKey = process.env.REACT_APP_CT_PROJECT_KEY;
const clientId = process.env.REACT_APP_CT_CLIENT_ID;
const clientSecret = process.env.REACT_APP_CT_CLIENT_SECRET;

const requestBuilder = createRequestBuilder({ projectKey });
const client = createClient({
  middlewares: [
    createAuthMiddlewareForClientCredentialsFlow({
      host: authUrl,
      projectKey: projectKey,
      credentials: {
        clientId: clientId,
        clientSecret: clientSecret,
      },
    }),
    createHttpMiddleware({ host: apiUrl }),
  ],
});

export function Search() {
  const [results, setResults] = useState([]);
  const inputRef = useRef();

  const onEnter = async () => {
    const input = inputRef.current;
    const isValid = input.value.length >= 3;
    if (isValid) {
      try {
        console.log(requestBuilder.productProjectionsSearch);
        const response = await client.execute({
          uri: requestBuilder.productProjectionsSearch
            .text(input.value, 'en-US')
            .expand('categories[*]')
            .parse({})
            .build(),
          method: 'GET',
        });
        if (response.statusCode === 200) {
          const results = response.body.results;
          console.log(results);
          setResults(results);
        }
      } catch (e) {
        console.error(e);
      }
    }
  };

  const checkKey = (event) => {
    if (event.key === 'Enter') onEnter(event);
  };

  useEffect(() => {
    const input = inputRef.current;
    if (input == null) return;
    input.addEventListener('keyup', checkKey);
    return () => {
      input.removeEventListener('keyup', checkKey);
    };
  }, [inputRef.current]);

  return (
    <>
      <input ref={inputRef} />
      <table>
        <tbody>
          <tr>
            <th>Product Image</th>
            <th>Product Name</th>
            <th>Product Category</th>
          </tr>
          {results.map((item) => (
            <tr key={item.id}>
              <td>
                <img src={item.masterVariant.images[0].url} width={100}></img>
              </td>
              <td>{item.name['en-US']}</td>
              <td>{item.categories[0].obj.name['en-US']}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
