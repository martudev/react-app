import React, { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';

import useFetch from 'use-http';

import messages from './messages';
import styles from './example1.module.scss';
import DefaultTemplate from '../../common/layouts/default';

const baseApiUrl = process.env.REACT_APP_BASE_API_URL;

export const Example1 = () => {
  const [movies, setMovies] = useState([]);
  const { get, response, loading, error } = useFetch(
    `${baseApiUrl}/search/shows?q=action`
  );

  async function loadInitialMovies() {
    const movies = await get();
    if (response.ok) setMovies(movies);
  }

  useEffect(() => {
    loadInitialMovies();
  }, []);

  return (
    <DefaultTemplate title={<FormattedMessage {...messages.title} />}>
      <h2>This page has the same layout that Home page has</h2>
      {error && <div className={styles.error}>Error!</div>}
      {loading && 'Loading...'}
      {movies.map((movie) => (
        <div key={movie.show.id}>{movie.show.name}</div>
      ))}
    </DefaultTemplate>
  );
};

Example1.displayName = 'Example1';
Example1.propTypes = {};

export default Example1;
