import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'example1.title',
    defaultMessage: 'Example 1 Page',
  },
});

export default messages;
