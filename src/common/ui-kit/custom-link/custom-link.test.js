import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import '@testing-library/jest-dom/extend-expect';

import { CustomLink } from './custom-link';

describe('CustomLink component', () => {
  test('full app rendering/navigating', () => {
    const history = createMemoryHistory();
    render(
      <Router history={history}>
        <CustomLink to="/lorem">Lorem ipsum</CustomLink>
      </Router>
    );
    // verify page content for expected route
    expect(history.location.pathname).toBe('/');

    expect(screen.getByText(/Lorem ipsum/i)).toBeInTheDocument();
    const leftClick = { button: 0 };
    userEvent.click(screen.getByText(/Lorem ipsum/i), leftClick);

    // check that the route changed to the new page
    expect(history.location.pathname).toBe('/lorem');
  });
});
