import React from 'react';
import { CustomLink } from '.';

export default {
  title: 'UI-Kit/Custom Link',
  component: CustomLink,
  argTypes: {
    to: {
      name: 'To',
      type: { name: 'string', required: true },
      defaultValue: '/my-link',
      description: 'demo description',
      control: {
        type: 'text',
      },
    },
    children: {
      name: 'Children',
      type: { name: 'string', required: true },
      defaultValue: 'Link',
      description: 'demo description',
      control: {
        type: 'text',
      },
    },
  },
};

const Template = (args) => <CustomLink {...args} />;

export const Default = Template.bind({});
Default.args = {
  to: '/my-link',
  children: 'Link',
};
