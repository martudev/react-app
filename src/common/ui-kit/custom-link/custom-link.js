import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export const CustomLink = (props) => (
  <Link to={props.to}>{props.children}</Link>
);

CustomLink.displayName = 'CustomLink';
CustomLink.propTypes = {
  /**
   * URL to go to (This is the href property)
   */
  to: PropTypes.string.isRequired,
  /**
   * Text or React component to use as Link
   */
  children: PropTypes.node.isRequired,
};

export default CustomLink;
