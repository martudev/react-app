import { defineMessages } from 'react-intl';

const messages = defineMessages({
  required: {
    id: 'textInput.error.required',
    defaultMessage: 'Required',
  },
});

export default messages;
