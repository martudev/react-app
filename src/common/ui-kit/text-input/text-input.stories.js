import React from 'react';
import TextInput from './text-input';

export default {
  title: 'UI-Kit/Text Input',
  component: TextInput,
  argTypes: {
    name: {
      name: 'Name',
      type: { name: 'string', required: true },
      defaultValue: 'firstname',
      description: 'Internal value for name property',
      control: {
        type: 'text',
      },
    },
    label: {
      name: 'Label',
      type: { name: 'string', required: false },
      defaultValue: 'Label',
      description: 'Value thats describe the field purpose',
      control: {
        type: 'text',
      },
    },
  },
};

const Template = (args) => <TextInput {...args} />;

export const Default = Template.bind({});
Default.args = {
  name: 'firstname',
  label: 'Label',
};
