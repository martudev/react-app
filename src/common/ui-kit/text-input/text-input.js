import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import { useIntl } from 'react-intl';

import styles from './text-input.module.css';
import messages from './messages';

export const TextInput = (props) => {
  const intl = useIntl();

  const { label, name, id, ...rest } = props;

  const [field, meta] = useField(props);
  return (
    <div>
      {label && <label htmlFor={id || name}>{label}</label>}
      <input
        id={id || name}
        className={styles['text-input']}
        {...field}
        {...rest}
        data-testid={name}
      />
      {meta.touched && meta.error ? (
        <div data-testid={`errors-${name}`} className={styles.error}>
          {intl.formatMessage(messages[meta.error.toLowerCase()])}
        </div>
      ) : null}
    </div>
  );
};

TextInput.displayName = 'TextInput';
TextInput.propTypes = {
  /**
   * Field name, for internal purpose
   */
  name: PropTypes.string.isRequired,
  /**
   * Label that describe the field
   */
  label: PropTypes.string,
};

export default TextInput;
