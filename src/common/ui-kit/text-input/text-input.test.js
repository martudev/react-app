import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import TextInput from './text-input';

import { IntlProvider } from 'react-intl';
import { Formik } from 'formik';

import en from '../../../i18n/en.json';
import es from '../../../i18n/es.json';

const messages = {
  en,
  es,
};
const DEFAULT_LOCALE = 'en';

const locale =
  (navigator.languages && navigator.languages[0]) || navigator.language;

const localeWithoutRegion = locale.toLowerCase().split(/[_-]+/)[0];

const messagesLocale =
  messages[localeWithoutRegion] || messages[locale] || messages[DEFAULT_LOCALE];

const setup = async () => {
  const name = 'pepeName';
  const label = 'First Name';

  const IntlWrapper = ({ children }) => {
    return (
      <IntlProvider locale={locale} messages={messagesLocale}>
        {children}
      </IntlProvider>
    );
  };

  const utils = render(
    <IntlWrapper>
      <Formik
        initialValues={{ [name]: '' }}
        validate={(values) => {
          let errors = {};

          if (!values[name]) {
            errors[name] = 'Required';
          }

          return errors;
        }}
      >
        <TextInput name={name} label={label} />
      </Formik>
    </IntlWrapper>
  );
  const input = await utils.findByTestId(name);
  return {
    input,
    ...utils,
  };
};

describe('TextInput component', () => {
  test('should rendering an input with 23 as value', async () => {
    const { input } = await setup();

    await waitFor(() => {
      fireEvent.change(input, { target: { value: '23' } });
    });
    expect(input.value).toBe('23');
  });

  test('should have validation error given input field is touched and error exists on form', async () => {
    const name = 'pepeName';
    const { input, findByTestId } = await setup();

    await waitFor(() => {
      fireEvent.blur(input);
    });

    const validationErrors = await findByTestId(`errors-${name}`);

    expect(validationErrors.innerHTML).toBe('Required');
  });
});
