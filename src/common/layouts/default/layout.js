import React from 'react';
import { ReactComponent as DGLogo } from '../../../assets/images/logo.svg';
import './layout.scss';

export const DefaultTemplate = ({ title, children }) => {
  return (
    <div>
      <header className="header">
        {/* <img src={logo} className="logo" alt="logo" /> */}
        <DGLogo className="logo" />
        <h1>{title}</h1>
        <ul>
          <li>
            <a className="link" href="https://reactrouter.com/">
              React router
            </a>
          </li>
        </ul>
      </header>
      {children}
    </div>
  );
};

DefaultTemplate.displayName = 'DefaultTemplate';
DefaultTemplate.propTypes = {};

export default DefaultTemplate;
